"""
Hodgkin-Huxley with Brian
Modified HH equations in Simmag group
"""

from brian import *
from brian import defaultclock
import matplotlib.pylab as plt
import matplotlib.gridspec as gridspec
import datetime

# it gives the name of the figure files with respect to time that the code runs
today = datetime.datetime.now()
today = today.strftime("%Y%m%d%H%M")

defaultclock.dt = 0.01*ms
duration = 2000.*ms

Ee = 0.*mV
Ei = -80.*mV
taue = 2.789*ms
taui = 10.49*ms

Cm = 1.*uF
gL = 0.3*msiemens
EL = 10.6*mV
ENa = 120.*mV
EK = -12.*mV
gNa = 120.*msiemens
gK = 36.*msiemens

eqs = '''
    dv/dt = (-gNa*m**3*h*(v-ENa)-gK*n**4*(v-EK)-gL*(v-EL)+ge*(Ee-v)+gi*(Ei-v)+I)/Cm : volt

    alpham = 0.1*(-vu+25.)/(exp((-vu+25.)/10.)-1.) : 1
    betam = 4.*exp(-(vu)/(18.)) : 1
    minf = alpham/(alpham+betam) : 1
    taum = 1./(alpham+betam)*ms : ms
    dm/dt = (minf-m)/taum : 1
    
    alphan = 0.01*(-vu+10.)/(exp((-vu+10.)/10.)-1.) : 1
    betan = 0.125*exp(-(vu)/(80.)) : 1
    ninf = alphan/(alphan+betan) : 1
    taun = 1./(alphan+betan)*ms : ms
    dn/dt = (ninf-n)/taun : 1

    alphah = 0.07*exp(-vu/20.) : 1
    betah = 1./(exp((-vu+30.)/10.)+1.) : 1
    hinf = alphah/(alphah+betah) : 1
    tauh = 1./(alphah+betah)*ms : ms
    dh/dt = (hinf-h)/tauh : 1

    dge/dt = (-ge)/taue : siemens
    dgi/dt = (-gi)/taui : siemens

    vu = v/mV : 1 # dimensionless membrane potential v
    I : amp
    '''

neuron = NeuronGroup(1,eqs)

neuron.v = -70.*mV


# Varied current is generated here, it depends on time.
t = linspace(0*second, duration, int(duration/defaultclock.dt))
current = clip(10*uA*t, 0, Inf)
neuron.I = TimedArray(current)

M = StateMonitor(neuron, 'v', record = True)

run(duration, report = 'text')

# set the figure font size
matplotlib.rcParams.update({'font.size' : 18})

f = plt.figure(num = None, figsize=(24,10), dpi = 80, facecolor = 'w', edgecolor = 'k')
gs = gridspec.GridSpec(2, 1, height_ratios = [12, 1])

ax0 = plt.subplot(gs[0])
xlim(0, duration/ms)
ax0.plot(M.times/ms, M[0]/mV, 'b')
title('HH with varied current')
ylabel('Membrane Potential')

ax1 = plt.subplot(gs[1])
xlim(0, duration/ms)
# allows you to define the range of y axes
plt.yticks(np.arange(min(current), max(current)*1e-02, np.average(current)*1e-02))
ax1.plot(current, 'r')
ylim(0, max(current)*1e-02)
ylabel('Current (ampere)')
xlabel('Time (ms)')

f.savefig('%s.png' % (str(today)))
f.savefig('%s.pdf' % (str(today)))
