"""
Hodgkin-Huxley with Brian
Classical HH equations
"""

from brian import *
from brian import defaultclock
import matplotlib.pylab as plt
import matplotlib.gridspec as gridspec

defaultclock.dt = 0.05*ms
duration = 2000.*ms

Ee = 0.*mV
Ei = -80.*mV
taue = 2.789*ms
taui = 10.49*ms

Cm = 1.*uF
gL = 0.1*msiemens
EL = -65.*mV
ENa = 55.*mV
EK = -90.*mV
gNa = 35.*msiemens
gK = 9.*msiemens

eqs = '''
    dv/dt = (-gNa*m**3*h*(v-ENa)-gK*n**4*(v-EK)-gL*(v-EL)+ge*(Ee-v)+gi*(Ei-v)+I)/Cm : volt
    m = alpham/(alpham+betam) : 1
    alpham = -0.1/mV*(v+35.*mV)/(exp(-0.1/mV*(v+35.*mV))-1.)/ms : Hz
    betam = 4.*exp(-(v+60.*mV)/(18.*mV))/ms : Hz
    dh/dt = 5.*(alphah*(1.-h)-betah*h) : 1
    alphah = 0.07*exp(-(v+58.*mV)/(20.*mV))/ms : Hz
    betah = 1./(exp(-0.1/mV*(v+28.*mV))+1.)/ms : Hz
    dn/dt = 5.*(alphan*(1.-n)-betan*n) : 1
    alphan = -0.01/mV*(v+34.*mV)/(exp(-0.1/mV*(v+34.*mV))-1.)/ms : Hz
    betan = 0.125*exp(-(v+44.*mV)/(80.*mV))/ms : Hz

    dge/dt = (-ge)/taue : siemens
    dgi/dt = (-gi)/taui : siemens

    I : amp
    '''

neuron = NeuronGroup(1,eqs)

neuron.v = -70.*mV


# Varied current is generated here, it depends on time.
t = linspace(0*second, duration, int(duration/defaultclock.dt))
current = clip(0.5*uA*t, 0, Inf)
neuron.I = TimedArray(current)

M = StateMonitor(neuron, 'v', record = True)

run(duration, report = 'text')

# set the figure font size
matplotlib.rcParams.update({'font.size' : 18})

f = plt.figure(num = None, figsize=(24,10), dpi = 80, facecolor = 'w', edgecolor = 'k')
gs = gridspec.GridSpec(2, 1, height_ratios = [12, 1])

ax0 = plt.subplot(gs[0])
xlim(0, duration/ms)
ax0.plot(M.times/ms, M[0]/mV, 'b')
title('HH with varied current')
ylabel('Membrane Potential')

ax1 = plt.subplot(gs[1])
xlim(0, duration/ms)
# allows you to define the range of y axes
plt.yticks(np.arange(min(current), max(current), np.average(current)))
ax1.plot(current, 'r')
ylim(0, 6e-8)
ylabel('Current (ampere)')
xlabel('Time (ms)')

plt.show()
